# INSTALLATION AND GETTING STARTED

### 1. Install prerequisites

APT based (Debian/Ubuntu):
`apt-get install libqt4-dev libqjson-dev gdb`

YUM based (CentOS):
`yum install libqt4-dev libqjson-dev gdb`

### 2. Build the binary
`qmake && make`

### 3. Test (Assuming Etherpad is running locally)
`./etherpad-stresstest http://127.0.0.1:9001/p/foo`

# Examples:
Basic run against localhost padId foo: 
`./etherpad-stresstest http://localhost:9001/p/foo`

Run with 10 lurking clients and 50 writers:
`./etherpad-stresstest --clients=lurk:10,write:50 http://localhost:9001/p/foo`

Run with verbosity/debug output set to the lowest setting: 
`./etherpad-stresstest --verbosity=0 http://localhost:9001/p/foo`

Run with a username and password set: 
`./etherpad-stresstest --user=John http://localhost:9001/p/foo`

Run for 3 seconds:
`./etherpad-stresstest --duration=3 http://localhost:9001/p/foo`


# Options:
`  --clients = JSON PAIR - Type of Client:Number of Clients IE lurk:10`

`  --duration = INTEGER - The duration to run the test for in seconds IE 10`

`  --verbosity = INTEGER - The verbosity of the output to the CLI (0 being lowest) IE 0`

`  --user = STRING - The username used to connect to a pad (You will be prompted for a password) IE john`


